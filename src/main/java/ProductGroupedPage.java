import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ProductGroupedPage extends BasePage {
    private WebDriver driver;

    @FindBy (xpath = "//div[@name='block_with_goods'][@class='pab-cell pab-img-45']")
    List<WebElement> brands;

    @FindBy (xpath = "//a[@href='https://rozetka.com.ua/notebooks/c80004/filter/producer=asus/']")
    WebElement brandAsus;

    @FindBy (xpath = "//a[contains(text(),'10 000 грн - 12 999 грн')]")
    WebElement price10000_12999FilterButton;

    public ProductGroupedPage(WebDriver driver) {
        super(driver);
        this.driver=driver;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public int numberOfBrands(){
        return brands.size();
    }

    public ProductListPage openProductListPage(){
        brandAsus.click();
        return new ProductListPage(driver);
    }

    public ProductPage selectPrice10000_12999Filter(){
        price10000_12999FilterButton.click();
        return new ProductPage(driver);
    }
}
