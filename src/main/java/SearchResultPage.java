import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.concurrent.TimeUnit;

public class SearchResultPage extends BasePage {
    private WebDriver driver;

    @FindBy (xpath = "(//Button[@class='btn-link-i'])[1]")
    private WebElement addToBasketButton;

    public SearchResultPage(WebDriver driver) {
        super(driver);
        this.driver=driver;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public ProductPage addFirstProductToBasket(){
        addToBasketButton.click();
        return new ProductPage(driver);
    }
}
