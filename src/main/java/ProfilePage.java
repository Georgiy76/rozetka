import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

public class ProfilePage extends BasePage {
    private WebDriver driver;

    @FindBy (xpath = "//button[contains(text(),'Подтвердить')]")
    WebElement confitmEmailButton;

    public ProfilePage(WebDriver driver){
        super(driver);
        this.driver=driver;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public boolean isConfirmEmailButton(){
        return confitmEmailButton.isDisplayed();
    }
}
