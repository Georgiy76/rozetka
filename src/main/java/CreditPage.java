import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class CreditPage extends BasePage{
    private WebDriver driver;
    Map<String,String> columnNameMap = new HashMap<String, String>();

    @FindBy (xpath = "//div[@class='credit-rules-title']")
    List<WebElement> creditRuleList;

    //можно сделать короче, но тогда не будет проверки на то что текст находиться в тайтле таблицы
    @FindBy (xpath = "//div[@class='rz-credit-block']/h2[contains(text(),'Условия кредитования')]")
    WebElement creditTermsTableTitle;

    @FindBy (xpath = "(//table[@class='rz-credit-terms-table'])[1]//tr[@class='rz-credit-terms-tr']")
    List<WebElement> creditTermsTableBaseCell;

    @FindBy (xpath = "//img[@title='Альфа-Банк']//ancestor::td/following-sibling::*[4]")
    WebElement ageRentAlphaBank;

    @FindBy (xpath = "//td[@class='rz-credit-terms-td rz-credit-terms-td-deposit']")
    List<WebElement> depositTermsList;

    public CreditPage(WebDriver driver) {
        super(driver);
        this.driver=driver;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public Integer getListCreditRules(){
        return creditRuleList.size();
    }

    public Boolean isCreditTermsTableTitle() {
        return creditTermsTableTitle.isDisplayed();
    }

    public Boolean isNeededTermISExist(String depositTerms, String period, String rate, String commision, String singleCommission, String constribution){
        for (int i = 1; i < creditTermsTableBaseCell.size()+1; i++) {
            try{
                if (driver.findElement(By.xpath("(//table[@class='rz-credit-terms-table']//td[@class='rz-credit-terms-td rz-credit-terms-td-deposit'])["+i+"][contains(text(),'"+depositTerms+"')]")).isDisplayed()){
                    if (driver.findElement(By.xpath("(//table[@class='rz-credit-terms-table']//td[@class='rz-credit-terms-td rz-credit-terms-td-period'])["+i+"][contains(text(),'"+period+"')]")).isDisplayed()){
                        if (driver.findElement(By.xpath("(//table[@class='rz-credit-terms-table']//td[@class='rz-credit-terms-td rz-credit-terms-td-rate'])["+i+"][contains(text(),'"+rate+"')]")).isDisplayed()){
                            if (driver.findElement(By.xpath("(//table[@class='rz-credit-terms-table']//td[@class='rz-credit-terms-td rz-credit-terms-td-commision'])["+i+"][contains(text(),'"+commision+"')]")).isDisplayed()){
                                if (driver.findElement(By.xpath("(//table[@class='rz-credit-terms-table']//td[@class='rz-credit-terms-td rz-credit-terms-td-single_commission'])["+i+"][contains(text(),'"+singleCommission+"')]")).isDisplayed()){
                                    if (driver.findElement(By.xpath("(//table[@class='rz-credit-terms-table']//td[@class='rz-credit-terms-td rz-credit-terms-td-contribution'])["+i+"][contains(text(),'"+constribution+"')]")).isDisplayed()){
                                        System.out.println(i+" true");
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }catch (NoSuchElementException e){
                System.out.println(i+" false");
            }
        }
        return false;
    }

    public String getValueFromTable(String packetName, String columnName){
        int i = 0;
        fillColumnNameMap();

        for (WebElement webElement : depositTermsList) {
            i++;
            if(webElement.getText().contains(packetName))
                return driver.findElement(By.xpath("(//table[@class='rz-credit-terms-table']//td[@class='"+columnNameMap.get(columnName)+"'])["+i+"]"))
                        .getText();

        }
        return "Something went wrong";
    }
    private void fillColumnNameMap(){
        columnNameMap.put("Льготный период без комиссий","rz-credit-terms-td rz-credit-terms-td-period");
        columnNameMap.put("Годовая ставка","rz-credit-terms-td rz-credit-terms-td-rate");
        columnNameMap.put("Ежемесячная комиссия после льготного периода","rz-credit-terms-td rz-credit-terms-td-commision");
        columnNameMap.put("Разовая комиссия","rz-credit-terms-td rz-credit-terms-td-single_commission");
        columnNameMap.put("Первоначальный взнос","rz-credit-terms-td rz-credit-terms-td-contribution");
    }

    public String isAgeRentAlphaBank(){
        //((Locatable)ageRentAlphaBank).getCoordinates();
        //((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();",ageRentAlphaBank);
        actions = new Actions(driver);
        actions.moveToElement(ageRentAlphaBank);
        actions.perform();

        System.out.println(ageRentAlphaBank.getText());
        return ageRentAlphaBank.getText();
    }

}
