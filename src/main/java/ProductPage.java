import org.openqa.selenium.WebDriver;
import java.util.concurrent.TimeUnit;

public class ProductPage extends BasePage {
    private WebDriver driver;

    public ProductPage(WebDriver driver){
        super(driver);
        this.driver=driver;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
}
