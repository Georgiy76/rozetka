import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ProductListPage extends BasePage {
    private WebDriver driver;

    @FindBy (xpath = "//div[@class='over-wraper']")
    List<WebElement> productListViewTile;

    @FindBy (xpath = "//div[@class='g-i-list-middle-part']")
    List<WebElement> productListViewList;

    @FindBy (xpath = "//img[@alt='Списком']")
    WebElement viewList;

//    @FindBy (xpath = "//a[@class='novisited g-i-more-link']")
    @FindBy (xpath = "//span[contains(text(),'Показать еще 16 товаров')]")
    WebElement gMoreProductsButton;

    @FindBy (xpath = "//a[contains(text(),'от 10000 до 12999 грн')]")
    WebElement price10000_12999Filter;

    By byProductListViewList = By.xpath("//div[@class='g-i-list-middle-part']");
    By byPrice10000_12999Filter = By.xpath("//a[contains(text(),'от 10000 до 12999 грн')]");
    public ProductListPage (WebDriver driver) {
        super(driver);
        this.driver=driver;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public int sizeOfProductListViewTile(){
        return productListViewTile.size();
    }


    public void listViewButton() {
    viewList.click();
    wait.until(ExpectedConditions.visibilityOfAllElements(productListViewList));

    }

    public int sizeOfProductListViewList(){
        return productListViewList.size();
    }

    public void gMoreProducts(){
        wait.until(ExpectedConditions.visibilityOf(gMoreProductsButton));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", gMoreProductsButton);
        /* actions.moveToElement(gMoreProductsButton)
                .click()
                .perform();*/
        gMoreProductsButton.click();
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(byProductListViewList,31));
    }
    public boolean isPrice10000_12999FilterIsSelected(){
//        wait.until(ExpectedConditions.visibilityOf(price10000_12999Filter));
        return price10000_12999Filter.isDisplayed();
    }

    public void removeFilter10000_12999() {
        price10000_12999Filter.click();
       // wait.until(ExpectedConditions.stalenessOf(price10000_12999Filter));
    }
}
