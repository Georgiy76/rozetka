import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.concurrent.TimeUnit;

public class RegistrationPage extends BasePage {
    private WebDriver driver;

    @FindBy (xpath = "(//input[@class='input-text auth-input-text'])[1]")
    private WebElement nameField;

    @FindBy (xpath = "(//input[@class='input-text auth-input-text'])[2]")
    private WebElement emailField;

    @FindBy (xpath = "(//input[@class='input-text auth-input-text'])[3]")
    private WebElement passwordField;

    @FindBy (xpath = "//button[@class='btn-link-i']")
    private WebElement submitRegistationFormButton;

    public RegistrationPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public ProfilePage newUserRegistration(String name, String email, String password){
        nameField.sendKeys(name);
        emailField.sendKeys(email);
        passwordField.sendKeys(password);
        submitRegistationFormButton.submit();
        return new ProfilePage(driver);
    }

}
