import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.concurrent.TimeUnit;

public class HomePage extends BasePage{
    private WebDriver driver;

    @FindBy (xpath = "//*[@href='https://my.rozetka.com.ua/signup/']")
    private WebElement registrationButton;

    @FindBy (xpath = "//a[@href='https://my.rozetka.com.ua/signin/']")
    private WebElement signinFormButton;

    @FindBy (xpath = "//*[@name='login']")
    private WebElement loginField;

    @FindBy (xpath = "//div[1]/input[@name='password']")
    private WebElement passwordField;

    @FindBy (xpath = "//form[@id='popup_signin']//button")
    private WebElement loginButton;

    @FindBy (xpath = "//*[@id='user_menu']/span[1]")
    private WebElement welcome;

    @FindBy (xpath = "//*[@id='header_user_menu']/li[12]/a")
    private WebElement logoutButton;


    public HomePage(WebDriver driver){
        super(driver);
        this.driver=driver;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public RegistrationPage openRegistrationPage(){

        registrationButton.click();
        return new RegistrationPage(driver);
    }

    public HomePage login(String email, String password) {
        signinFormButton.click();
        loginField.sendKeys(email);
        passwordField.sendKeys(password);
        loginButton.click();
        return this;
    }

    public HomePage logout(){
        welcome.click();
        logoutButton.click();
        return this;
    }

}
