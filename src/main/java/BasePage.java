import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    WebDriver driver;
    Actions actions;
    WebDriverWait wait;

    @FindBy (xpath = "//input[@class='rz-header-search-input-text passive']")
    private WebElement searchField;

    @FindBy (xpath = "//button[@class='btn-link-i js-rz-search-button']")
    private WebElement searchBatton;

    @FindBy (xpath = "//li[@class='hub-i hub-i-cart']/div[@name='splash-button']")
    private WebElement basketButton;

    @FindBy (xpath = "//*[contains(text(),'Корзина пуста')]")
    private WebElement basketIsEmptyLocator;

    @FindBy (xpath = "//a[@href='https://rozetka.com.ua/credit/']")
    private WebElement openCreditPageButton;

    @FindBy (xpath = "//h2[contains(text(),'Вы добавили товар в корзину')]")
    private WebElement cartTitle;

    @FindBy (xpath = "//a[@name='close']")
    private WebElement closeCartPopup;

    @FindBy (xpath = "//span[contains(text(),'Каталог товаров')]")
    private WebElement productCatalogButton;

    @FindBy (xpath = "//a[@data-title='Ноутбуки и компьютеры']")
    private WebElement notebooksPCProducts;

    @FindBy (xpath = "//li[@class='f-menu-pop-l-i']/a[@href='https://rozetka.com.ua/notebooks/c80004/']")
    private WebElement notebooksButton;

    public BasePage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    public SearchResultPage searchProduct(String product){
        searchField.sendKeys(product);
        searchBatton.click();
        return new SearchResultPage(driver);
    }

    public BasePage openBasketView() {
        basketButton.click();
        return this;
    }

    public boolean basketIsEmpty() {
        basketButton.click();
        try{
            basketIsEmptyLocator.isDisplayed();
        }catch (NoSuchElementException e){
            System.out.println("Basket isn't empty");
            return false;
        }
        System.out.println("Basket is empty");
        return true;
    }

    public CreditPage openCreditPage(){
        openCreditPageButton.click();
        return new CreditPage(driver);
    }

    public boolean isProductAddedCartPopup() {
        return cartTitle.isDisplayed();
    }

    public ProductGroupedPage openProductGroupedPage(){
        actions = new Actions(driver);
        actions.moveToElement(productCatalogButton)
                .moveToElement(notebooksPCProducts)
                .click(notebooksButton)
                .perform();

        return new ProductGroupedPage(driver);
    }

}
