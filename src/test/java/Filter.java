import org.testng.Assert;
import org.testng.annotations.Test;
import setup.BaseTest;

public class Filter extends BaseTest{
    HomePage homePage;
    ProductGroupedPage productGroupedPage;
    ProductListPage productListPage;

    @Test
    public void isSelectedRightFilter(){
        homePage = new HomePage(driver);
        homePage.openProductGroupedPage();
        productGroupedPage = new ProductGroupedPage(driver);
        productGroupedPage.selectPrice10000_12999Filter();
        productListPage = new ProductListPage(driver);
        Assert.assertTrue(productListPage.isPrice10000_12999FilterIsSelected(), "'от 10000 до 12999 грн' filter doesn't exist");
        productListPage.removeFilter10000_12999();
        //Assert.assertFalse(productListPage.isPrice10000_12999FilterIsSelected(), "'от 10000 до 12999 грн' filter exists");
    }
}
