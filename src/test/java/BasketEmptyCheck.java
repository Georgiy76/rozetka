import org.testng.Assert;
import org.testng.annotations.Test;
import setup.BaseTest;

public class BasketEmptyCheck extends BaseTest {
    HomePage homePage;

    @Test
    public void basketEmptyCheck(){
    homePage = new HomePage(driver);
    Assert.assertTrue(homePage.basketIsEmpty());
    }
}
