import org.testng.Assert;
import org.testng.annotations.Test;
import setup.BaseTest;

public class NumberOfBrandsOnPage extends BaseTest{
    HomePage homePage;
    ProductGroupedPage productGroupedPage;

    @Test
    public void counntOFBrandsOnPage(){
        homePage = new HomePage(driver);
        homePage.openProductGroupedPage();
        productGroupedPage = new ProductGroupedPage(driver);
        Assert.assertTrue(productGroupedPage.numberOfBrands() == 8, "Number of the brands isn't 8");

    }
}
