import org.testng.Assert;
import org.testng.annotations.Test;
import setup.BaseTest;


public class RegisterNewUser extends BaseTest {
    HomePage homePage;
    RegistrationPage registrationPage;
    ProfilePage profilePage;

    @Test (priority=1)
    public void registerNewUser(){
        homePage = new HomePage(driver);
        homePage.openRegistrationPage();
        registrationPage = new RegistrationPage(driver);
        registrationPage.newUserRegistration(NAME,EMAIL,PASSWORD);
        profilePage = new ProfilePage(driver);
        Assert.assertTrue(profilePage.isConfirmEmailButton());
     }







}
