import org.testng.annotations.Test;
import setup.BaseTest;

public class Login extends BaseTest{
    HomePage homePage;
    @Test
    public void login() throws InterruptedException {
        homePage = new HomePage(driver);
        homePage.login(EMAIL, PASSWORD);
    }
}
