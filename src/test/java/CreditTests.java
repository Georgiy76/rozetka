import org.testng.Assert;
import org.testng.annotations.Test;
import setup.BaseTest;

public class CreditTests extends BaseTest{
    HomePage homePage;
    CreditPage creditPage;

    @Test
    public void creditTests(){
        homePage = new HomePage(driver);
        homePage.openCreditPage();
        creditPage = new CreditPage(driver);
        Assert.assertTrue(creditPage.getListCreditRules() == 4 );
    }

    @Test
    public void  isCreditTermsTableTitle(){
        homePage = new HomePage(driver);
        homePage.openCreditPage();
        creditPage = new CreditPage(driver);
        Assert.assertTrue(creditPage.isCreditTermsTableTitle());
    }

    @Test
    public  void isNeededTermExist(){
        homePage = new HomePage(driver);
        homePage.openCreditPage();
        creditPage = new CreditPage(driver);
        Assert.assertTrue(creditPage.isNeededTermISExist("0,01% на 15 месяцев","15","0,01","2,5","-","от 0"));
    }

    @Test
    public  void isItemExist(){
        homePage = new HomePage(driver);
        homePage.openCreditPage();
        creditPage = new CreditPage(driver);
        Assert.assertEquals(creditPage.getValueFromTable("Кредит с компенсацией до 35% IQ","Разовая комиссия"), "1,99");
    }

    @Test
    public void isAgeRentAlphaBank(){
        homePage = new HomePage(driver);
        homePage.openCreditPage();
        creditPage = new CreditPage(driver);
        Assert.assertEquals(creditPage.isAgeRentAlphaBank(),"21-70");
    }
}
