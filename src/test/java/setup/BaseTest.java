package setup;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import java.io.File;
import java.io.IOException;


public class BaseTest {
    private static ChromeDriverService service;
    private static ChromeOptions option;
    public static WebDriver driver;
    public final static String NAME = "MegaPuxar";
    public final static String EMAIL = "MegaPuxar31@yopmail.com";
    public final static String PASSWORD = "MegaPassword99";
    public final static String PRODUCT = "Galaxy s8";

    @BeforeMethod
    public void load() throws IOException {
        service = new ChromeDriverService.Builder()
                .usingDriverExecutable(new File("drivers","chromedriver.exe"))
                .usingAnyFreePort()
                .build();
        service.start();
        option = new ChromeOptions();
        option.addArguments("--start-maximized");
        driver = new ChromeDriver(option);
        driver.get("https://rozetka.com.ua/");
    }


    @AfterMethod
    public void teardown(){
        driver.quit();
        driver = null;
    }


}
