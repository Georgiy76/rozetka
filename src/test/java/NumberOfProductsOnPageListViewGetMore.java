import org.testng.Assert;
import org.testng.annotations.Test;
import setup.BaseTest;

public class NumberOfProductsOnPageListViewGetMore extends BaseTest{
    HomePage homePage;
    ProductGroupedPage productGroupedPage;
    ProductListPage productListPage;

    @Test
    public void counntOFProductsOnPage(){
        homePage = new HomePage(driver);
        homePage.openProductGroupedPage();
        productGroupedPage = new ProductGroupedPage(driver);
        productGroupedPage.openProductListPage();
        productListPage = new ProductListPage(driver);
        productListPage.listViewButton();
        productListPage.gMoreProducts();
        Assert.assertTrue(productListPage.sizeOfProductListViewList() == 32, "List doesn't have 32 products");

    }
}
