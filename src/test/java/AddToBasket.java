import org.testng.Assert;
import org.testng.annotations.Test;
import setup.BaseTest;


public class AddToBasket extends BaseTest {
    HomePage homePage;
    SearchResultPage searchResultPage;

    @Test
    public void addToBasket() throws InterruptedException {
        //homePage = PageFactory.initElements(driver, HomePage.class);
        homePage = new HomePage(driver);
        homePage.searchProduct(PRODUCT);
        searchResultPage = new SearchResultPage(driver);
        searchResultPage.addFirstProductToBasket();
        Assert.assertTrue(searchResultPage.isProductAddedCartPopup());
    }
}
